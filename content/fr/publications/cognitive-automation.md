---
title: "L'automatisation des tâches cognitives complexes"
date: 2010-05-05
slug: "automatisation-taches-cognitives"
draft: false
---

Et son impact sur la société.

<!--more-->

#### L'IA: bon coup ou mauvais coup?

L'humanité est une espèce technologique. Retracer l'histoire de l'humanité est
presque impossible sans se pencher sur ses inventions.
Si certaines de ces inventions
sont clairement des bons coups (comme la musique), et que certaines sont clairement des mauvais
coups (comme la bombe atomique), la plupart représentent une mixture des
deux (comme l'industrialisation, qui apporte à la fois un confort matériel
indéniable, mais aussi des problèmes de pollution qui semblent de nos jours
insurmontables).

L'humanité est aussi une espèce sociale, ce qui explique en grande partie sa passion
dévorante pour les technologies de l'information et de la communication:
l'écriture, l'imprimerie, la poste, le téléphone et, bien sûr, l'informatique.

L'informatique représente un point de rupture dans l'histoire de l'humanité.
Pour la première fois, les machines créées par les humains investissent la
sphère intellectuelle en accomplissant une grande variété de tâches cognitives,
le plus souvent à une vitesse fulgurante.  Si encore récemment ces tâches
étaient relativement simples (comme des calculs comptables), la montée de
l'intelligence artificielle (IA) entraîne la possible automatisation d'une
myriade de tâches cognitives complexes (comme la composition d'une pièce de
musique).  Il est clair que l'humanité est au beau milieu d'un
changement de paradigme aussi profond que l'a été l'invention et la
popularisation de l'écriture.

Ce qui soulève la question: est-ce que l'IA est un bon coup, un mauvais coup, ou
une mixture des deux?  D'un côté, les transhumanistes comme <a
href="https://fr.wikipedia.org/wiki/Raymond_Kurzweil" target="_blank">Ray
Kurzweil</a> prétendent que l'IA peut sauver l'humanité en la transformant en
une nouvelle espèce, qui aurait fusionnée avec ses technologies.  D'un autre
côté, des scientifiques de renommée internationnale, dont <a
href="https://www.lemonde.fr/pixels/article/2018/03/14/l-intelligence-artificielle-pourrait-etre-la-pire-ou-la-meilleure-chose-qui-soit-arrivee-a-l-humanite-estimait-hawking_5270820_4408996.html"
target="_blank">Stephen Hawking</a>, affirment que l'IA représente une menace
existentielle pour l'humanité et se sont montrés favorables à un moratoire sur
certaines de ces technologies, <a
href="https://www.ledevoir.com/societe/632400/l-onu-appelle-a-la-prudence-avec-l-intelligence-artificielle"
target="_blank">comme y réfléchit l'ONU</a>.  Qui sait, peut-être vaudrait-il
mieux bannir l'IA par traité, comme dans le cas des armes nucléaires?

En attendant, si on veut comprendre ces enjeux, il faut adopter la troisième
posture et se demander en quoi l'intelligence artificielle pourrait devenir un
pilier de notre épanouissement individuel et collectif, ou par quels moyens,
exactement, ces technologies viendraient les menacer.  Une piste de réponse
importante réside dans la capacité qu'aura l'humanité à contrôler ces
technologies et à les démocratiser.

#### L'IA fait reculer la démocratie de 26 siècles?

Ma réflexion porte donc sur l'intersection entre les technologies de
l'information et la démocratie, une intersection qui est à notre époque
en pleine réorganisation.

Analysée à travers la lorgnette de la pensée informatique («computational
thinking»), la démocratie est un immense système de gestion de l'information et
de prise de décision. Le système repose sur les personnes qui y occupent
différentes fonctions, bien sûr, mais aussi en très grande partie sur des
procédures et des lois qui, pour fonctionner, doivent être écrites. Par
exemple, lorsque la Cour suprême tranche dans un dossier épineux, on peut dire
dans un premier temps que la décision est celle des juges ayant rendu le
verdict.  En y regardant de plus près, on voit que ce verdict est fortement
influencé par les lois précédemment écrites, par la jurisprudence et par les
arguments présentés par les avocats.  

En termes informatiques, on dirait que les personnes impliquées 
ont fourni la puissance de calcul permettant de traiter l'information
pertinente, mais que ce sont les règles de droit (telles qu'interiorisées et
interprétées par les avocats et les juges) qui ont, ultimement, décidé du
verdict.  Pour utiliser une image un peu bancale: chaque cerveau est un
ordinateur, et les règles de droit sont des logiciels. Avec le même cerveau, on
peut obtenir des résultats bien différents si on change le logiciel.

Évidemment, les gens ne sont pas des robots et l'autonomie intellectuelle des
personnes impliquées est très importante, en particulier afin de
détecter les cas où les textes de loi sont inadéquats (auquel cas nos
législateurs peuvent les retravailler).  En même temps, sans textes de
loi pour encadrer le travail des juges, leurs décisions risqueraient d'être
arbitraires.  Il s'agit là d'une exigence de la démocratie qui remonte au 5ième
siècle avant notre ère, alors que les plébéiens ont obtenu que soit définit par écrit le
pouvoir des consuls romains.

Et bien, si on remplaçait aujourd'hui les juges par des intelligences
artificielles, on risquerait fort de faire reculer la démocratie de 26 siècles!
En effet, la très vaste majorité des IA conçues par apprentissage machine (ou
apprentissage profond) n'ont pas la capacité d'expliquer leurs décisions ou de
consigner par écrit leur raisonnement.  Elles n'auraient pas non plus la
capacité de détecter des problèmes dans les lois, ce qui demande une pensée
transversale qu'aucune IA connue ne maîtrise.  Finalement, on ne pourrait pas
vérifier de façon indépendante que les décisions des juges artificiels sont
sages, ou, au minimum, conformes à la loi.  La population serait contrainte de
les accepter commes telles.  L'idée est terrifiante, surtout quand on sait que
ce genre d'IA commet parfois des erreurs aberrantes (p.ex. identifier un panneau de
signalisation comme un frigo plein de boissons).  Imaginez un juge
artificiel qui signe des milliers de verdict à chaque heure, et qui, de temps
en temps, sans qu'on ne sache trop pourquoi ni lequel, produit un verdict
complètement erroné.  Même si, en moyenne, cette IA s'avèrait
supérieure aux juges humains, voulez-vous vraiment d'un tel système de
justice? 

(Par opposition, s'il est vrai que les humains se trompent
souvent, leurs erreurs ont tendances à s'approcher de solutions
raisonnables).

#### La démocratie, une affaire de littératie

Le système complexe qu'est la démocratie a évolué en même temps que la
littératie.  Il y a eu des moments forts, bien sûr (comme la Magna Carta ou la
constitution américaine), mais il s'agit somme toute d'un processus de
raffinements successifs où l'humanité a appris (et apprend toujours) à utiliser
la littératie comme un outil de prise de décision.


<i>[à compléter]</i>



#### Démocratie 2.0: une affaire de codératie?

L'informatique est une bête d'une nature bien différente que l'écriture, et les
processus de prise de décision conçus pour fonctionner avec le support de la littératie sont maintenant à risque de devenir obsolètes.

L'enjeu principal à comprendre est le suivant: l'informatique permet d'automatiser la prise de décision.
Autrement dit, l'informatique n'est pas du tout un medium neutre. L'information qu'on y consigne sera traitée,
modifiée et réinventée, tout ça sans aucune intervention humaine. Il ne faut pas faire l'erreur de penser que l'informatique 
permet à la société de «faire la même chose qu'avant», mais simplement de le faire plus facilement (ou encore à distance).

À partir du moment où nos processus de décision reposent sur l'informatique, une grande partie du contrôle
qu'on pourrait avoir sur ces processus est repris et assumé par les logiciels. Et le comportement de ces logiciels 
est dicté par le code. Or, avec environs 1% de la population apte à lire et écrire ce code, il va sans dire que le pouvoir propre à l'informatique est extrêmement concentré (probablement au point d'être incompatible avec la démocratie). 

(Imaginez une société avec un taux de littératie à 1%: que resterait-il de la démocratie telle qu'on la connaît?)

<i>[à compléter]</i>

Est-on en train de découvrir, comme l'ont fait les plébéiens il y a 26 siècles, une nouvelle exigence pour la démocratie?
Cette fois-ci, plutôt que demander à ce que les lois soient écrites et publiées, il faut exiger que le code source (qui est déjà écrit), soit
rendu public (ou à tout le moins rendu accessible à des experts aptes à l'analyser).

<!--
Une exigence similaire à celle faite par les plébiens il y a 26 siècles, mais cette fois-ci non pas sur l'écriture du code
-->


<!--

Contre cette concentration du pouvoir informatique, la solution est simple: enseigner la programmation au plus grand nombre.
Si l'humanité pouvait elever son taux de codératie autour de 20% -- pour prendre un chiffre au hasard -- l'informatique serait d'emblée beaucoup plus représentative (alors qu'aujourd'hui 90% des programmeurs sont des hommes).
-->


<!--

L'histoire de la démocratie: 

Or, l'invention de l'écriture remonte à tellement loin qu'il est difficile pour
nous d'imaginer qu'à une certaine époque, l'humanité a dû s'y adapter et c'est
toute l'organisation sociale qui a changé.



Assez robuste à l'automatisation des tâches manuelles, mais la réalité est tout autre
Avec l'IA, automatiser des tâches cognitives subtiles.


Mais tout ça est en train de changer de nouveau. Depuis l'avènement de l'IA,
l'humanité est face à un changement de paradigme aussi important que
l'invention de l'écriture, et les méthodes de prise de décision ne tiennent
plus.  Maintenant que les machines prennent des décisions (bonnes ou
mauvaises), on peut parier que l'organisation de la société pourrait changer de
façon aussi profonde qu'à la suite de l'invention de l'écriture.


Avec la montée de la nouvelle IA, la portée réelle de l'informatique est
devenue claire pour tout le monde: les tâches cognitives qu'on croyait réservé

Nouvelle révolution industrielle.

Avant: les machines automatise des tâches manuelles. Déjà, l'impact pour l'économie a été profond: révolution industrielle.

Mais les techniques de prises de décision n'avait pas été significativement touchée. Reposaient principalement sur la littératie.



-->

