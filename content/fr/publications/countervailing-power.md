---
title: "La liberté informatique, un nouveau contre-pouvoir"
date: 2019-08-05T03:22:41-04:00
slug: "contre-pouvoir"
draft: false
---

Publié dans Le Devoir, 16 octobre 2021.

<!--more-->

<br>

<center>
<div style="font-size:80%; padding:10px; border:solid pink 2px">Mathieu Bergeron.
<a href="https://www.ledevoir.com/opinion/libre-opinion/640794/libre-opinion-la-liberte-informatique-un-nouveau-contre-pouvoir" target="_blank">La liberté informatique, un nouveau contre-pouvoir.</a>. Le Devoir. 16 octobre 2021.
</div>
</center>


<br>


<br>
<br>

Suite aux commentaires engendrés par la publication du texte ci-haut, deux clarifications s'imposent:

1. La liberté informatique se veut un droit collectif compatible avec la libre entreprise
1. L'enseignement de la programmation au primaire et au secondaire est un projet tout à fait réaliste, comme l'illustrent certaines <a href="https://www.ledevoir.com/societe/education/636462/a-l-ecole-jean-nicolet-on-code-des-la-maternelle-et-on-adore-ca" target="_blank">initiatives récentes 🔗</a>.

<br>

## Un droit collectif, un projet réaliste


Premièrement, il faut savoir qu'il existe déjà plusieurs entreprises qui ont
fait le pari d'ouvrir complétement leur code et qui réussissent néanmoins à
générer des profits. Par exemple, l'entreprise américaine Red Hat déclarait un
profit annuel net de 437 millions de dollars au moment de son rachat par IBM en
2018, tout en investissant des sommes considérables dans le développement de
Linux, un système d'exploitation entièrement libre sur lequel repose une grande
partie de l'infrastructure d'Internet.  Plus près de chez nous, l'entreprise
Savoir-Faire Linux, fondée à Montréal en 1999, fournit de l'emploi à une
cinquantaine de personnes en suivant un modèle similaire. Imaginez la fierté de
ces entrepreneurs/euses, qui réussissent à la fois à soutenir la prospérité
économique de leur ville ou pays, tout en contribuant à bâtir un patrimoine
technologique commun pour toute l'humanité.

En même temps, la liberté informatique n'en demande pas tant et pourrait fort
bien reposer sur un accès au code plus limité, en autant qu'il s'agisse de code
source (c'est-à-dire les fichiers qu'utilisent les programmeurs/euses pour créer une
application).  Par exemple, on peut imaginer une approche similaire aux
brevets, où le code source d'un système informatique deviendrait
automatiquement public après un certain nombre d'années. Cela permettrait de
faire pression sur les géants de l'informatique afin que leurs algorithmes
soient plus éthiques et équitables, tout en favorisant la concurrence et
l'esprit d'innovation. Selon le cadre législatif envisagé, un segment non
négligeable de l'industrie pourrait s'y montrer favorable (surtout que l'idée
de code source ouvert y est devenue très populaire depuis quelques années).

Deuxièmement, si l'enseignement public de la programmation est essentiel, ce
n'est pas uniquement afin d'outiller chaque enfant en vue d'un parcours
scolaire qui, à l'avenir, risque fort de demander une familiarité avec le code.
La portée est plus profonde.  De la même façon que le pouvoir émancipateur de
la littératie ne réside pas dans les lettres, mais bien dans l'utilisation de
la lecture et de l'écriture comme des outils pour la pensée, le pouvoir de la
codératie ne réside pas dans le code comme tel, mais bien dans la pensée
informatique («computational thinking»), une aptitude qui dépasse l'utilisation
des outils informatiques et qui concerne plutôt la compréhension de leur
fonctionnement interne.  C'est grâce à cette pensée informatique que les citoyens
pourraient suivre les comptes-rendus d'expert·es visant à dévoiler les
comportements cachés de plateformes comme Facebook (à condition, bien sûr, que
ces expert·es puissent accéder à leur code source).

En même temps, il ne s'agit pas de surcharger le cursus scolaire. On peut
facilement imaginer que la programmation soit intégrée à l'apprentissage des
mathématiques, [voire des langues]({{<ref research-project>}}). Il ne s'agit pas
que chaque enfant devienne  programmeur/euse, mais bien de s'assurer que les
citoyens de demain soient aptes à débattre du fonctionnement interne de
systèmes informatiques qui, pour le meilleur et pour le pire, se sont taillé
une place centrale dans l'économie et la vie civique.

