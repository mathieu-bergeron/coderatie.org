---
title: "L'impact environnemental de l'informatique"
date: 2019-08-02T03:22:41-04:00
slug: "impact-environnemental"
draft: false
---

Et comment le diminuer.

<!--more-->



### Une course vers l'avant néfaste et évitable

L'industrie informatique fait piètre figure en terme de pratiques 
écologiques:

1. appareils jetables ou volontairement impossibles à réparer
1. obsolescence programmée
1. <a href="https://ici.radio-canada.ca/nouvelle/1101238/montreal-logiciel-materiel-libre-francois-croteau" target="_blank">menottage informatique 🔗</a> privant
   les petites et moyennes organisations d'adopter des solutions plus efficaces
1. <a href="https://ici.radio-canada.ca/nouvelle/1074385/iphone-lent-ralentit-apple-ios-obsolescence-programmee" target="_blank">mises à jours forcées 🔗</a> qui incitent à remplacer prématurément le matériel informatique

On dirait que l'humanité s'est donné le défi d'emplir les dépotoirs d'appareils
électroniques désuets.  Ce qui est le plus triste là dedans, c'est qu'à
l'exception de quelques enthousiastes, les citoyen·nes ne semblent même pas souhaiter
cette course vers l'avant, ces changements incessants dans les interfaces
graphiques, dans les façons de travailler, dans la terminologie à employer,
dans l'émergence et la déchéance de nouvelles plateformes informatiques.  J'ai
souvent l'impression que plusieurs consommateurs suivent ces changements par
dépit, parce que c'est la chose à faire, pour ne pas passer pour un dinosaure,
mais qu'au fond ce qu'ils souhaitent vraiment, c'est de se sentir en contrôle.

<!--
À ce triste tableau, il faut ajouter que l'utilisation d'un ordinateur est une
source d'anxiété pour plusieurs (même chez les jeunes), et ce malgré les
meilleurs efforts des géants de l'informatique à produire des systèmes
informatiques supposément intuitifs.
-->


### Ralentir le rythme

Tout le monde s'accorde donc pour dire qu'en informatique, tout bouge tellement
vite. Vous serez alors surpris d'apprendre que j'écris ces
lignes en utilisant `vim`, un éditeur de texte créé il y a 29 ans à partir de
`vi`, lequel date de 1976... soit avant ma naissance!

<center>
<img width="90%" src="/images/vim.png"/>
</center>

<br>



La codératie change la relation qu'on peut avoir avec l'informatique.  Plusieurs
programmeurs en viennent à percevoir les appareils informatiques comme de
simples supports permettant d'écrire et d'exécuter du code.  C'est à travers le
code que ces derniers s'expriment.  C'est à travers le code qu'une programmeuse
se sentira en contrôle de son ordinateur, qu'elle sentira qu'elle peut en tirer
le maximum.  L'apparence des boutons devient rapidement secondaire et la
nouveauté de l'appareil importe beaucoup moins. 

D'ailleurs, les «vieux outils» ne sont pas rares chez les informaticien·nes.
Bien souvent, écrire ou modifier du code ne nécessite pas d'outils
ultra-performants (il s'agit après tout de simples fichiers textes), et le
potentiel émancipateur de la codératie n'a rien à voir avec l'outil qu'on
utilise pour écrire le code (de la même façon que le potentiel émancipateur de
la littératie n'a rien à voir avec le fait d'utiliser une plume plutôt qu'un stylo).

Avec un peu de code TeX (un langage créé en 1978 et encore utilisé
de nos jours), on peut produire des images comme celles-ci:

<center>
<figure>
<img width="200px" src="/images/exemple-tex/sudoku-3d-cube.png"/>
<figcaption style="padding-top:1rem;">source: <a target="_blank" href="https://texample.net/tikz/examples/sudoku-3d-cube">texample.net/tikz/examples/sudoku-3d-cube 🔗</a></figcaption>
</figure>
</center>
<br>
<br>

<center>
<img width="400px" src="/images/exemple-tex/diagramme_algo.png"/>
</center>

Non seulement la qualité typographique des images et documents produits en TeX
(et son successeur LaTeX, créé en 1983) dépasse souvent celle des suites bureautiques
modernes, mais l'utilisation de code afin de produire des documents présente
plusieurs avantages. Par exemple:

1. On peut écrire des procédures afin d'automatiser certaines mises en forme.

1. On peut générer plusieurs versions d'un même document.  La deuxième image,
   par exemple, est générée à partir d'un fichier contenant les coups d'une
   partie d'échecs.  Le même code peut être utilisé pour générer une image
   similaire, mais correspondant à une autre partie.
   
1. On peut bénéficier du code écrit par autrui.  Par exemple, on compte en ce
   moment pas moins de 6132 paquets LaTeX publiés sur <a
   href="https://www.ctan.org/" target="_blank">ctan.org 🔗</a>.  Dans bien des
   cas, ces paquets permettent de réaliser des documents sophistiqués avec un
   minimum d'effort de codage. D'ailleurs, j'ai utilisé un paquet existant
   pour afficher chaque échiquier, je n'ai pas eu besoin de progammer cet
   affichage moi-même.

Le dernier point est crucial et explique comment un taux de codératie plus
élevé pourrait parvenir à freiner la course folle de l'informatique.  Avec le
code, on entre dans une logique de partage et d'accumulation des connaissances,
et on sort de la logique de la mise à jour constante des outils.  Ces
changements incessants, d'ailleurs, apportent rarement une réelle plus-value
en terme de possibilités pour l'utilisateur.  Par opposition,  les langages TeX
et LaTeX n'ont pas changé significativement depuis des décennies, mais leur
potentiel d'expressivité continue encore de croître, grâce au partage de
code.


### Lutter contre l'inflation informatique

Réorganiser l'informatique autour de l'utilisation du code et d'outils simples
permettrait aussi de réduire notre consommation de ressources.  Pour travailler sur ce
texte, `vim` me «coûte» environs 6% de la puissance de mon processeur et 4MB
de mémoire vive.  En comparaison, un éditeur moderne comme VSCode utilise
jusqu'à 40% de mon processeur et plus de 1000MB de mémoire, soit 250 fois
plus que `vim`!

On peut aussi s'étonner d'apprendre que VSCode utilise environs 6% de mon
processeur même lorsque je ne suis pas en train de modifier le fichier (contre
0% pour `vim` qui, au repos, ne fait rien). Ce genre de gaspillage est
malheureusement typique des outils modernes.  Un document vide ouvert en Word
demande plus de 50MB de mémoire vive et quelques points de pourcentage du
processeur.

L'impact environnemental de ces quelques points de pourcentage est sans doute
négligeable, mais ces gaspillages, en s'accumulant, finissent par justifier
l'achat de matériel informatique plus performant. Et c'est là que le coût
écologique peut s'avérer majeur.  D'autant plus que les outils modernes sont
réputés pour devenir de plus en plus gourmand d'une version à l'autre.  Par
opposition, les outils plus simples comme `vim` ou LaTeX ont plutôt tendance à
consommer la même quantité de ressources d'une version à l'autre, voire parfois
à en consommer moins (p.ex. `xelatex` est une version plus récente de LaTeX
qui, tout en supportant le même langage, permet d'économiser jusqu'à 7% en
ressources).


### La liberté de décentraliser les données

La soif de ressource des outils modernes soulève une autre question: mais
qu'est-ce que VSCode et Word peuvent bien faire à consommer de la puissance de
calcul alors que je ne suis même pas en train de les utiliser?

Une partie de la réponse réside dans le fait que ces outils envoient
des <a href="https://code.visualstudio.com/docs/getstarted/telemetry"
target="_blank">données d'utilisation 🔗</a> à des serveurs contrôlés par
l'entreprise qui les a créé (dans ce cas-ci Microsoft).  Aux ressources
gaspillées sur mon ordinateur, il faut donc ajouter une charge accrue pour les
appareils réseau d'Internet et, surtout, la construction et la maintenance de
centres de données capable d'accumuler toute cette information.

Et c'est là que le bât blesse.  Il est étourdissant d'imaginer la quantité
d'énergie dédiée à des centres de données dont le bénéfice pour
l'humanité est souvent douteux. Contrairement au gaspillage de ressources locales,
l'impact écologique de ces centres de données est direct et non négligeable.

(Il y a évidemment des cas où un centre de données est une bonne solution,
p.ex. lorsque qu'un superordinateur est partagé entre plusieurs organisations.)

Je n'ai aucun doute que si les citoyen·nes pouvaient faire valoir leurs droits
plus efficacement en matière d'informatique, les outils modernes ne
collecteraient pas autant de données et permettraient d'utiliser, au besoin,
une mixture entre l'infonuagique et les ressources locales. Le bloquant n'est
pas technique (il existe déjà des solutions de ce genre comme NextCloud), mais
bien dans le rapport de force inégal qui existe entre les citoyen·nes et les
petites organisations d'un côté, et les géants comme Microsoft et Google de
l'autre.




### La liberté de réparer et de réutiliser

Si on veut rendre l'informatique plus éco-responsable, il est donc crucial de
«baisser les attentes» par rapport à la nouveauté, d'apprendre à se satisfaire
de matériel informatique qui, s'il n'est pas dernier cri, est fonctionnel là où
ça compte.  Encore une fois, le bloquant n'est pas technique.  Le système
d'exploitation libre Linux est souvent utilisé pour redonner vie à des vieux
ordinateurs qui ne sont plus assez performants pour rouler la version la plus
récente de Windows.  Pour une multitude d'usages, dont une grande variété
de tâches de programmation, un vieil ordinateur est amplement suffisant. 

D'ailleurs, j'écris ces lignes sur un ordinateur portable vieux d'une douzaine
d'années.  Pour les applications que j'utilise en ce moment, je ne
vois absolument aucune différence de performance entre ce vieux portable et
l'ordinateur tout neuf que j'ai au bureau. (Je suis en Linux avec un
système de fenêtrage simple, l'éditeur `vim` dans un terminal et Firefox pour
visionner les pages HTML générées par l'outil Hugo.)

Sur ce portable vieux modèle, je peux facilement changer la batterie, la
mémoire et le disque dur (et le clavier, et possiblement l'écran pour quelqu'un
qui serait plus habile que moi avec ces choses). Mais sur la plupart des portables plus récents
(et pour la vaste majorité des téléphones et des tablettes électroniques), ces
composants sont soudés ou collés, ce qui rend l'appareil virtuellement
impossible à réparer. 

Encore une fois, le problème n'est pas technique, comme l'illustre l'existence
de produits tout à fait modernes et néanmoins réparables (p.ex. les portables
<a href="https://frame.work/ca/en" target="_blank">Framework 🔗</a> ou les
téléphones <a href="https://www.fairphone.com/fr/" target="_blank">Fairphone
🔗</a>).  L'enjeu est que les piliers habituels de la démocratie semblent
ineffectifs pour soit i) forcer les géants de l'informatique à adopter des
pratiques plus écologiques ou ii) permettre à la population de mieux percevoir
les aspects les plus néfastes de l'informatique grand public, laquelle est
basée sur l'utilisation d'applications supposément conviviales plutôt que
l'émancipation des utilisateurs à travers le code.

Avec un taux de codératie plus élevé, c'est tout un segment de la population
qui, ayant développé une relation bien différente avec l'informatique,
risquerait fort de prévilégier des appareils conçus pour être robustes et
durables. 

(Pas nécessairement par souci écologique, mais peut-être simplement afin
d'avoir le contrôle sur leur ordinateur, ce qui passe par l'accès au code et la
possiblité de le réparer. Néanmoins, parions que la plus-value écologique sera
un incitatif majeur pour plusieurs et, surtout, soulignons que le résultat est
bénéfique pour l'environnement indépendamment des motivations sous-jacentes.)

On peut même imaginer qu'un jour, en réaction à ce nouveau marché,
les géants de l'informatique investiraient une partie de leurs efforts de
recherche et développement non pas pour créer des nouveautés, mais bien pour
rendre les outils existants plus légers, plus malléables pour qui sait
programmer, voire plus compatibles avec des appareils moins puissants,
mais conçus pour se tenir loin des dépotoirs le plus longtemps possible.



<!--

Le phénomène de l'obsolescence programmée illustre bien le problème.  Imaginez
un fabriquant d'imprimante qui décide qu'après 1000 impressions, la cartouche
d'encre doit être changée pour une neuve (qu'elle soit vide ou non).  La
cartouche contient déjà une puce servant à informer l'imprimante du niveau
d'encre. L'imprimante contient déjà un processeur pour assurer la
communication avec l'ordinateur.  Ajouter un peu de code qui compte le nombre
d'impressions et bloque l'impression jusqu'à l'insertion d'une nouvelle
cartouche est une affaire de rien. Mais comment l'empêcher? 

La presse avertit les consommateurs de ce genre de tactique depuis un long
moment déjà et ça ne semble pas changer grand chose.  On peut imaginer un
recour collectif contre le fabriquant en vertu de la Loi sur la protection du
consommateur. En supposant que cette loi couvre un cas comme celui-ci, comment
prouver que le fabriquant est fautif? Le comportement de l'imprimante est dicté
par le code informatique, et ce dernier est enfermé en format binaire dans un
composant électronique.  Même en réussissant à l'extraire, en analyser le
comportement de code binaire est extrêmement difficile et risque fort de ne pas
nous informer 
-->





<!--


## Démocratie 2.0

-->







