---
title: "L'informatique émancipatrice"
date: 2020-08-08T05:50:08-04:00
slug: "informatique-emancipatrice"
categories: "L'informatique émancipatrice"
---

Avec le code, les possibilités d'expérimentation et de création explosent.

<!--more-->

### La littératie numérique ne suffit pas

La littératie numérique concerne l'utilisation de logiciels grand public, 
mais exclut la compréhension du code (pour laquelle nous utilisons le terme codératie).

Les logiciels grand public utilisent des analogies graphiques (comme le bureau, les dossiers, etc.) pour nous permettre
de contrôler l'ordinateur.
Le code est au contraire un langage textuel.
L'analogie avec les langues naturelles est éclairante:

* Littératie numérique
    * savoir utiliser l'ordinateur, c'est comme savoir lire et écrire uniquement avec des pictogrammes et des émojis.
* Codératie
    * savoir programmer, c'est comme pouvoir lire et écrire dans une langue naturelle comme le français.

On voit clairement comment la littératie numérique équivaut, en terme informatique, à un genre d'analphabétisme fonctionnel.


### La puissance des langages

Il y a eu plusieurs tentatives pour remplacer les langages de programmation par quelque chose qui peut semble à première vue plus simple.
La grande majorité (la totalité?) ont échoué.

Comme pour les langages naturels, la puissance des langages de programmation
vient de la possibilité de créer facilement de nouvelles phrases (ou nouveaux
programmes) à partir de règles relativement simples.  C'est la possibilité de
combiner plusieurs petits éléments afin de créer une plus grande phrase (un
plus grand programme) que les systèmes plus simples n'arrivent pas à répliquer
(en effet, au delà d'exemples minuscules, ces systèmes «simples» deviennent plus
complexes que le code).

Vouloir remplacer les langages de programmation par un système soi-disant convivial, c'est comme dire:

* On ne devrait pas écrire nos lois en français, on devrait utiliser uniquement des pictogrammes et des émojis.

### Langage et liberté

Pour qui sait déjà programmer, l'idée de remplacer complétement les langages de
programmation par des systèmes graphiques est un non-sens, une perte de liberté
incongrue qui provoque un indescriptible de frisson de dégoût. 

Pourquoi est-ce que ça devrait être différent pour les citoyen·nes? Pourquoi les
citoyen·nes devraient se contenter d'une méthode vastement inférieure d'interaction
avec les ordinateurs?
Imaginez l'idée de remplacer le français par un système de pictogrammes
(ou par une <a href="https://fr.wikipedia.org/wiki/Novlangue" target="_blank">novlangue</a>&nbsp;🔗 à la 1984). Ne ressentez-vous pas intensément (voire physiquement) 
l'immense perte de liberté que cela représente?

Ça ne veut pas dire que nous devrions tout le temps utiliser le code pour
intéragir avec nos ordinateurs.  Les systèmes graphiques ont leur
place, bien sûr. Mais il y a fort à parier que leur place serait beaucoup plus petite
si nous pouvions instaurer un taux de codértie plus élevé et développer
des langages de programmation plus faciles à [apprendre et à enseigner]({{<ref
code-for-everyone>}}).


### À qui profite votre littératie numérique?

La courte histoire de l'informatique est remplie de projets éducatifs
visant à redonner du pouvoir aux citoyen·nes (je pense à Smalltalk et
Logo, par exemple). C'est assez révélateur que les géants de l'informatique ont
historiquement très peu investi dans ce genre de projet et ont à peu près jamais
cherché à rendre leurs outils utilisables à travers du code, préférant
offrir uniquement des interfaces graphiques.

De leur point de vue, l'utilisateur idéal possède une
certaine compétence (une certaine littératie), mais n'est pas apte à comprendre
en profondeur les outils qu'il utilise, ni à créer des alternatives. 

C'est l'absence de codératie chez le grand public qui donne autant de pouvoir
aux géants de l'informatique et qui permet aux *startups* de financer le 
développement de systèmes visant sciemment à [perturber un secteur de l'économie]({{<ref direct-economy>}}).

L'existence même du logiciel libre (conçu par et pour des programmeurs/programmeuses, et souvent
distribué gratuitement) montre comment la codératie rééquilibre
le rapport de force entre les citoyen·nes et le capitalisme numérique.

### Exemple

Pour qui sait programmer, utiliser un outil graphique est souvent peu aléchant. Si je peux écrire 
<a href="#" data-toggle="modal" data-target="#grosFichiers">
une ligne de code
</a>
pour trouver tous les fichiers plus gros que 100MB, l'idée d'ouvrir un explorateur de fichier graphique et de les chercher via une fonctionnalité de recherche (ou pire «à la main») semble bien ridicule. Surtout que le pouvoir du code est d'inventer des commandes à la volée, au moment où j'en ai besion.

<div class="modal fade" id="grosFichiers" tabindex="-1" role="dialog" aria-labelledby="grosFichiersLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="grosFichiersLabel">Fichiers plus gros que 100MB</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<pre>
<code>
$ find . -size +100M
</code>
</pre>
        </div>
        </div>
    </div>
</div>



À partir du moment où je sais comment lister les fichiers plus gros que 100Mb, je peux

1. <a href="" data-toggle="modal" data-target="#sauvegarderListe">Sauvegarder cette liste</a>


1. <a href="" data-toggle="modal" data-target="#videos">Déterminer quels fichiers sont des vidéos</a>

1. <a href="" data-toggle="modal" data-target="#ouvrirVideos">Ouvrir les vidéos une à la fois</a>

1. <a href="" data-toggle="modal" data-target="#ouvrirVideosEnMemeTemps"> Ouvrir toutes les vidéos en même temps</a> (déconseillé)

<!--

        $ for v in $(cat v.txt); do vlc $video& done

-->


<div class="modal fade" id="sauvegarderListe" tabindex="-1" role="dialog" aria-labelledby="sauvegarderListeLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sauvegarderListeLabel">Sauvegarder une liste de gros fichiers</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<pre>
<code>
$ find . -size +100M > gros_fichiers.txt
</code>
</pre>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="videos" tabindex="-1" role="dialog" aria-labelledby="videosLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:1000px">
            <div class="modal-header">
                <h5 class="modal-title" id="videosLabel">Déterminer quels fichiers sont des vidéos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<pre>
<code>
$ for gf in $(cat gros_fichiers.txt); do file -i $gf | grep -q video && echo $gf; done > videos.txt
</code>
</pre>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ouvrirVideos" tabindex="-1" role="dialog" aria-labelledby="ouvrirVideosLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ouvrirVideosLabel">Ouvrir les vidéos une à la fois</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<pre>
<code>
$ for v in $(cat videos.txt); do vlc $v; done
</code>
</pre>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ouvrirVideosEnMemeTemps" tabindex="-1" role="dialog" aria-labelledby="ouvrirVideosEnMemeTempsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ouvrirVideosEnMemeTempsLabel">Ouvrir les vidéos une à la fois</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<pre>
<code>
$ for v in $(cat videos.txt); do vlc $v & done
</code>
</pre>
        </div>
        </div>
    </div>
</div>


Ce genre d'invention de petites commandes est un type de programmation appelé scriptage.
Le langage utilisé ici est `sh`, qui a été créé en 1979 est et encore couramment utilisé de nos jours.
