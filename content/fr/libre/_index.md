---
title: "Quelques logiciels libres"
draft: false
slug: "libre"
---

<table>
<tr>
<th>
Domaine
</th>
<th>
Logiciels
</th>
</tr>

<tr>
<td>
Navigateurs Web
</td>
<td>

<a href="https://www.mozilla.org/fr/firefox/new/" target="_blank">Firefox</a>🔗

<a href="https://brave.com/" target="_blank">Brave</a>🔗

</td>
</tr>

<tr>
<td>
Infonuagique (sondages, formulaires, cartes, photos, fichiers, etc.)
</td>
<td>

<a href="https://framasoft.org" target="_blank">Framasoft</a>🔗

<a href="https://nextcloud.com/fr/" target="_blank">NextCloud</a>🔗


</td>
</tr>

<tr>
<td>
Bureautique
</td>
<td>

<a href="https://www.libreoffice.org/" target="_blank">LibreOffice</a>🔗


</td>
</tr>

<tr>
<td>
Dessin et retouches photo
<td>

<a href="https://www.gimp.org/" target="_blank">Gimp</a>🔗

<a href="https://krita.org/fr/" target="_blank">Krita</a>🔗

<a href="https://inkscape.org/fr/" target="_blank">Inkscape</a>🔗


</td>

</tr>


<tr>
<td>
Capture vidéo et diffusion (screencast, streaming)
<td>

<a href="https://obsproject.com/fr" target="_blank">OBS studio</a>🔗

</td>
</tr>

<tr>
<td>
Montage vidéo
<td>

<a href="https://kdenlive.org/fr/" target="_blank">Kdenlive</a>🔗

<a href="https://www.openshot.org/fr/" target="_blank">OpenShot</a>🔗

</td>
</tr>

<tr>
<td>
Mixage audio
<td>

<a href="https://www.audacityteam.org/" target="_blank">Audacity</a>🔗

<a href="https://ardour.org/" target="_blank">Ardour</a>🔗

</td>
</tr>

<tr>
<td>
Dessin et animation 3D
<td>

<a href="https://www.blender.org/" target="_blank">Blender</a>🔗

</td>
</tr>

<tr>
<td>
Création de jeux vidéos
<td>

<a href="https://godotengine.org/" target="_blank">Godot</a>🔗

</td>
</tr>


<tr>
<td>
Installer Linux (et redonner vie à un vieil ordi)
<td>

Très vieil ordi: <a href="https://puppylinux-woof-ce.github.io/" target="_blank">Puppy Linux</a>🔗

Vieil ordi: <a href="https://lubuntu.me/" target="_blank">Lubuntu</a>🔗

Ordi plus récent: <a href="https://pop.system76.com/" target="_blank">Pop!Os</a>🔗, <a href="https://ubuntu.com/download/desktop" target="_blank"/>Ubuntu Desktop LTS</a>🔗 ou <a href="https://manjaro.org/" target="_blank">Manjaro</a>🔗, 

</td>
</tr>



</table>
