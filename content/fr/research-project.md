---
title: "Texto: programmation en langue naturelle pour novices"
date: 2020-03-29T06:52:00-04:00
draft: false
slug: "projet-de-recherche"
---

<br>

<center>
<div style="font-size:80%; padding:10px; border:solid pink 2px">
Ce projet de recherche fait l'objet d'une demande de subvention FRQNT -- recherche collégiale.
<br>
Les résultats seront annoncés en avril 2022.</div>
</center>

<br>
<br>




### Résumé

L'intelligence artificielle (IA) est à la fois un sujet d'émerveillement et
d'inquiétude. La performance de ces technologies est souvent impressionnante,
mais leur fiabilité peut laisser à désirer, et leurs décisions sont trop
souvent impossibles à expliquer. Une approche prometteuse pour obtenir des IA
plus fiables et plus explicables est de générer du code informatique afin d'en
expliciter le comportement, qui pourrait dès lors être inspecté et modifié. Si
le code informatique pouvait ensuite être expliqué dans une langue naturelle
comme le français, il s'agirait là d'une avancée majeure vers la
démocratisation de l'informatique. 


Dans cet ordre d'idée, nous proposons
d'explorer la relation entre le français et le code informatique. À court
terme, notre projet consiste à créer un outil pédagogique, appelé Texto, qui
sera basé sur la notion de programmation en langue naturelle, où les
instructions que l'ordinateur doit exécuter sont spécifiées en français.
L'objectif immédiat est donc de créer une approche pédagogie innovatrice pour
l'enseignement de la programmation, un domaine où la demande a beaucoup
augmenté récemment. À moyen terme, un tel outil permettra de collecter des
données concrètes sur la relation entre le français et le code informatique. De
telles données sont essentielles à la création et à l'amélioration d'IA
génératrices de code, ce qui pourrait contribuer à rendre l'intelligence
artificielle plus fiable et plus explicable. 

Or, la programmation en langue
naturelle n'est pas une approche généraliste. Un texte libre, usant de la
pleine richesse d'une langue naturelle comme le français, n'est pas assez
précis pour être exécuté par un ordinateur. Il est ainsi nécessaire d'obtenir
un texte contrôlé, qui utilise un vocabulaire restreint et une grammaire
stricte. Au coeur de l'outil Texto que nous proposons, il y a donc une cascade
de traitements qui tente de transformer le texte libre, fourni par
l'étudiant.e, en texte contrôlé. Si la transformation réussit, l'outil pourra
générer du code informatique et en visualiser l'exécution. Si la transformation
échoue, l'outil va plutôt produire une liste de suggestions que l'étudiant.e
pourra suivre afin de rendre son texte plus contrôlé. Cette cascade de
traitements implique diverses techniques d'intelligence artificielle que le
projet Texto va harnacher et adapter. Une partie de cette adaptation provient
d'un.e enseignant.e, qui doit écrire des tutoriels d'apprentissage de la
programmation spécifiquement conçus pour l'outil Texto.

Les résultats souhaités
pour le projet incluent l'utilisation de Texto comme outil pédagogique au
Collège Montmorency, puis dans d'autres
institutions à travers la francophonie. 
Nous débuterons aussi une collecte de
données concernant les textes produits par les étudiant.es, en relation avec le
code informatique généré par l'outil. (À noter que cette collecte s'effectuera
avec le consentement des étudiant.es et n'inclura aucune information
personnelle). Ces données pourraient contribuer à la création d'IA génératrice
de code et, à terme, à des IA plus fiables et explicables. Finalement, nous
espérons aussi explorer le potentiel de la programmation en langue naturelle en
termes d'accessibilité, par exemple en combinaison avec les logiciels de
synthèse vocale qu'utilisent certaines personnes atteintes de déficience
visuelle. 


<!--




Afin de rendre l'apprentissage de la programmation plus accessible, nous
proposons une approche où l'étudiant·e pourra dicter des instructions en
français et visualiser les instructions que l'ordinateur exécute pour y
répondre.
Nous espérons ainsi permettre à l'étudiant·e de réaliser son
apprentissage de façon plus graduelle.

### Problématique abordée 

L'apprentissage de la programmation se divise en deux compétences majeures: 

1. maîtriser la syntaxe formelle du code 
1. se créer un modèle mental de comment l'ordinateur exécute ce code, étape par étape.

Typiquement, l'étudiant·e doit acquérir ces compétences à partir d'exemples et
exercices tirés de langages de programmation professionnels comme Java.
La marche est haute, car ces langages ne sont pas conçus pour l'enseignement:

1. la syntaxe est touffue et capricieuse
1. comprendre le modèle d'exécution requiert des connaissances préalables inaccessibles aux étudiant·es (soit le
fonctionnement interne de l'ordinateur et la théorie des langages de
programmation).

### Approche proposée

Notre projet vise à rendre l'apprentissage de la programmation plus graduel.
Dans un premier temps, une intelligence artificielle permettra à l'étudiant·e
de dicter des commandes en langue naturelle (un peu comme les assistants sur
les téléphones multifonctions: «Je veux dessiner un cercle»). Ces commandes
seront interprétées comme du code dans un langage de programmation spécialisé
pour l'enseignement, mais syntaxiquement proche de Java. L'étudiant·e pourra
explorer à son rythme cette traduction en code, lui permettant ainsi de se
familiariser graduellement avec sa syntaxe formelle.

Dans un deuxième temps, l'exécution du code sera visuelle, dans la tradition
des langages de programmation pédagogiques comme Logo, Smalltalk et, plus
récemment, Scratch. L'étudiant·e pourra plus facilement se créer un modèle
mental du fonctionnement du programme. Encore une fois, l'étudiant·e pourra
explorer à son rythme la relation entre cette visualisation et les détails de
l'exécution du code.

Notre approche se distingue des outils pédagogiques existants de deux façons:

1. l'utilisation d'intelligence artificielle pour rendre l'outil aussi
accessible que possible 
1. l'idée de guider l'étudiant·e pas à pas vers
l'apprentissage d'un langage généraliste comme Java.

-->
