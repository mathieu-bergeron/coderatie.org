---
title: "Software should be Empowering"
date: 2020-08-21T05:50:08-04:00
---

With code, possibilities for experimentation and creation explode.
<!--more-->

<br>
<br>
<br>

*Yet to be translated from [French](https://coderatie.org/publications/informatique-emancipatrice/)*
