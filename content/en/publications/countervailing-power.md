---
title: "Software freedom, a new countervailing power"
date: 2019-08-05T03:22:41-04:00
slug: "countervailing-power"
draft: false
---

Published in Le Devoir, October 16 2021.

<!--more-->

<center>
<div style="font-size:80%; padding:10px; border:solid pink 2px">Mathieu Bergeron.
<a href="https://www.ledevoir.com/opinion/libre-opinion/640794/libre-opinion-la-liberte-informatique-un-nouveau-contre-pouvoir" target="_blank">La liberté informatique, un nouveau contre-pouvoir.</a>. Le Devoir. October 16 2021.
</div>
</center>


<br>

In a way, software has already been wonderfully democratized, that is rendered
accessible to the masses (especially since the advent of smartphones).  But in
the deeper sense of the word, which implies to redistribute power amongst the
population, today's software is mainly ademocratic, if not anti-democratic
(as the industry relies too often on right privations: privacy violations,
devices artificially made impossible to repair, decisions made by black box
algorithms that cannot explain their reasoning).

In the walled fiefdom that is Facebook, citizens do not sway a lot of
control over the kind of content served to them by an artificial intelligence
that, as recent events show, is far from being an enlightned ruler.  If
regulating Facebook and other cyberspace dictatorships is becoming a necessity,
we must not think that it enough: their nature will not change and regulations
will at best act as some harm reduction process.

Indeed, as the american jurist Lawrence Lessig pointed out already twenty years
ago: in the cyberspace, code is law.  The behavior of apps and algorithms that
have embedded themselves in our lives is dictated by source code.  Programming
languages are formalisms specialized in the precise description of how very
complex systems evolve over time. No text of law could hope to regulate such
systems (unless it is written in code).

If we want to create a more democratic cyberspace (or perhaps simply less
toxic), we must learn to deliberate about source code: is it public? Who can
understand it?  Who can modify it? It is no surprise that ethical
concerns are far less pressing in the free software community, where source
code is always public and where comments and modifications of that code are
encouraged. Let's wager that if Facebook's source code could have been 
inspected and analyzed by independant experts from the get go,
the worst aspects of the platform would never have taken hold.

In view of the immense power of software giants, we need much more than
regulations. What we need is no less than a new countervailing power: software
freedom. The same way that press freedom is based on access to information and
litteracy, software freedom would be based on access to source code and
coderacy (the capacity to understand that code).  We should write laws in order
to fortify our democracy for the age of algorithms, but these laws should
pertain to source code accessibility and to the place of computer programming
in mandatory public instruction.

Democracy as we know it cannot exist without a decent level of litteracy.  For
the democracy of tomorrow to emerge, we must give our children the gift of
coderacy.
