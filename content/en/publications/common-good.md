---
title: "Common Good in the Age of Algorithms"
date: 2019-08-04T03:22:41-04:00
slug: "bien-commun"
draft: false
---

Published in Liberté, no329

<!--more-->


<center>
<span style="font-size:80%; padding:10px; border:solid pink 2px">Bergeron, M. (2021). <a href="https://revueliberte.ca/article/1552/Le_bien_commun_%C3%A0_l_%C3%A8re_des_algorithmes" target="_blank">Le bien commun à l’ère des algorithmes.</a> Liberté, (329),47–49</span>
</center>

<br>
<br>

The nineties were worrisome for anyone making a living from chess: 
popularity was declining amongst young people and chess algorithms were rising.
Machines, playing with ruthless efficiency, never blundering, were now
defeating human champions, even if chess software had the reputation of lacking
a sense of strategy.  Could it be that raw calculations were sufficient to
defeat grand masters and their subtle understanding of the game?  In this
angstful atmosphere, IBM organized the famous match opposing the world champion
Garry Kasparov to the super computer Deep Blue: a battle for the survival of
chess itself, for beauty,  against algorithmic brutishness.  A battle that
humanity lost.  Or rather, that is what we thought.

<center>
<img src="/images/common-good/graphe_un.png" width="49%"/>
<img src="/images/common-good/graphe_deux.png" width="49%"/>
</center>
<br>

After Kasparov's defeat, the popularity of chess surprisingly skyrocketed, as
shown by data we have extracted from FIDE's website.  What happened?  Did chess
algorithms support that growth?  If so, we can imagine three reasons: i) we can
learn from chess algorithms, ii) we can control them and modify them and iii)
they did not lead to a concentration of wealth.  Inspired by chess, we
articulate a vision of the common good which is compatible with a future where
software will fully encompass our economic and civic lives.  We envision a
redistribution of its benefits along three key axis: i) meaning, ii) freedom,
and iii) revenue.  To differentiate our vision from the values tacitly
promoted by today's software giants, we compare it to the Uber taxi service
(while noting that any other well-known
platform would do: Airbnb, Youtube, Facebook, etc.).

### The machine becomes the (grand) master

With Deep Blue's victory over Garry Kasparov, chess experienced its artificial
intelligence (AI) crisis, which climax was undoubtedly the thirty-sixth move of
the second game, where the machine rejected the option to capture two pawns,
choosing to capture a single pawn and hence preserve a long-term strategic
advantage.  Kasparov was so surprised by the move that he accused IBM of
cheating -- an accusation now obsolete as modern chess algorithms are
well-known to largely surpass human abilities, including in terms of strategic
vision.  The chess community soon learned to appreciate the surprising
competence of algorithms, not only to discover the best moves, but also to
explain them.  Chess software is now a precious pedagogical tool, used e.g.  by
amateurs to understand the games of grand masters.

Chess players have discovered by experience what philosopher Daniel C. Dennett
explains brilliantly (most recently in *From Bacteria to Bach and Back.
The Evolution of Minds*): raw calculations, if leveraged by the right
algorithm, will be able to create and manipulate semantic information (e.g. the
meaning of a sentence or the identification of a person by its facial
features).  Of course, most algorithms are not like that: they will not create anything surprising
and they have nothing to teach humans.  However, with the rise of the new
AI, based on Big Data and machine learning, a world saturated with algorithms
able to create and manipulate meaning has never seemed so probable.  If we wish
to keep our own understanding of the world, instead of depending on the
understanding of machines, we must make sure that the meaning created by
algorithms is intelligible and  accessible to the public.  It is only at those
two conditions that AI can contribute to humanity's culture, as it does in the
case of chess.


<center>
<img src="/images/common-good/cadran_sens.png" width="90%"/>
</center>
<br>


Unfortunately, machine learning produces systems that are efficient, but
opaque.  Without additional research and development (R&amp;D), such systems do
not have the ability to explain their decisions.  Uber's *Upfront
pricing* algorithm, for example, proposes a price as soon as the user orders a
taxi ride.  However, the client is never informed of what motivates that price.
She can accept the price proposed by the algorithm, or choose to pay at the end
of the ride, according to the taximeter (which in the case of Uber, applies
fares that can vary during the day).  As the algorithm tries to offer prices
that are higher than the taximeter's, it must construct some sort of
understanding of urban traffic.  At the same time, for clients to accept those
prices frequently, the algorithm must also model clients' willingness to pay
(e.g. according to their history).  Since Uber does not allow users to freely
experiment with their algorithm, we cannot determine how much the understanding
that the algorithm generates is made intelligible, i.e. how much added meaning
it extracted from the data.  It is however certain that this added meaning,
if it exists, is never shared with the public.


### Taking back control

Chess software are numerous and easily accessible, which was already the case
when Deep Blue was introduced. This assuredly made it easier to accept the
strategic superiority of chess algorithms.  In the chess world, humans choose
when and how to delegate decisions to AI (which is one of the Asilomar ethical
principles).  We can experiment with multiple algorithms and compare them.  For
computer programmers, it is even possible to access the source code of many
chess programs, hence modifying how they behave and gaining even more control
and possibilities for exploration.

Unfortunately, the software freedom that the chess community enjoys if almost
never offered to the public at large.  The majority of applications allow for
very little experimentation and the control that users have is very limited, in
particular over how data is shared, and algorithms applied.  If civic life was
some day to become exclusively orchestrated through computer systems that offer
so little freedom (and that secretly applies meaning-manipulating algorithms),
our democracy could very well become an empty shell, a ritual dislocated from
important decisions (which would be taken in part by machines designed and
controlled by particular interests).  To avoid such a collapse, we need to
follow the lead of the free software and open source movements, and adopt
software that respects and promotes the freedom of its users.  We need to
access the source code of our applications and to learn how to publicly
deliberate about its behavior.

<center>
<img src="/images/common-good/cadran_liberte.png" width="90%"/>
</center>
<br>

As the jurist Lawrence Lessig observed in his essay \textit{Code Is Law}, the
real possibilities of software are defined by code.  The behavior of algorithms
is too complex and changing to hope to constrain it by human laws.  It is by
accessing the source code of a system that we can verify that it does not
harbor malicious intent or reproduce its creator's biases.  With this in mind,
let us wager that if independent investigators would be given the power to
inspect Uber's source code and publish their impressions, their *Upfront
pricing* system would behave a lot more like advertised (a way to plan
transportation costs), and less like a scheme to maximize revenue.

Freedom of press can act as a model here.  It leverages access to information
and literacy to create a countervailing power. So could software freedom, but
this time leveraging access to source code and *coderacy* (the ability
to understand this code).

#### Sharing more than revenue

Shortly after its victory, the super-computer Deep Blue was modified by IBM in
order to reuse its computing power on new tasks such as financial analysis.
Perhaps IBM judged that creating meaning for chess would not generated enough
profits? Or perhaps the presence of free software alternatives made
commercialization too risky.  Whatever the reasons, the chess community was not
reorganized around a platform (a proprietary technology that becomes necessary
for some human activity).

The chess world dodged a bullet.  For a given domain, a platform
possesses near total control on the redistribution of meaning, freedom, and of
course revenue. Google is a good example in the domain of information search,
and Uber aims at doing the same for the transportation industry.  As argued
by Jaron Lanier in *Who Owns the Future*, the concentrating power of
platforms is so strong that it already represents a major factor of income
inequality and, in the future, combined with the increasing automation of
production means, will represent a serious threat to the stability of our
economies.  To reverse that trend, Lanier proposes to integrate micropayments
directly into information transferred over the Internet.  Users would be
compensated for the data they generate and that platforms greatly need to
improve their algorithms.  On the other hand, many
software services could not anymore be offered free of charge.

<center>
<img src="/images/common-good/cadran_revenus.png" width="90%"/>
</center>
<br>

This could be a saving grace, as startups bet on the illusion that their
services are free to install themselves as platforms.  Financed by venture
capital, startups appear at first to be very generous, even altruistic.  On the
contrary, their explicit intention is to disrupt a given domain of activity, to
change its culture and habits, to the point where using their software become
virtually unavoidable.  When successful, the monetization phase that follows
can be very aggressive.  We can think of Google's or Facebook's massive
siphoning of private data.  In the case of Uber, not only is the
*Upfront pricing* algorithm an effort to inflate prices, but it also
decreases the proportion of revenue that is shared with drivers (as drivers are
still paid according to the taximeter, even when the client chose the higher
price proposed by the algorithm).

Detecting that kind of programmed inequity is difficult if we are denied the
freedom to experiment in depth with software (worse, the target is
moving: e.g.  Uber regularly tests new ways to determine prices).  Without
access to meaning, we cannot plan ahead according to the kinds of
decisions that algorithms will take.  These decisions become opaque and we are
only subject to them (e.g.  Uber clients must to compose with hyper-variable
prices, without being able to anticipate them).

In short, the economic issue cannot be addressed in isolation.  If we hope to
benefit from software systems that share income fairly, we have to make
sure that those systems also share meaning and freedom.



#### Inverting Uber... and reinventing democracy?

To better imagine what software that supports the common good mike look like,
it is helpful to envision what the opposite of Uber could be.
That mirror image, let us call it Beru, 
would espouse the same objectives that Uber gives itself
publicly: organize transportation intelligently and share the
resulting income. However, Beru would use software and algorithms very differently.
 
With Beru, intelligence would not only be artificial: every decision made by
the system would be explained and users could debate  them and suggest
modifications.  The system would be designed to allow for external experts to
participate in data analysis and the responsibility of improving its algorithms
would be shared.  Citizens could defend their rights ahead of time, by
proposing modifications to the source code.  This way, major citizen concerns,
such as climate change, could be integrated directly into transportation algorithms.
Similarly, principles for income sharing
would be defined, debated, and revised collectively, directly in the source
code.  It would be up to the community to decide on how to split
revenue between drivers and R\&D aimed at improving the system. In short, 
Beru would enable the public to take control over the machines.

With the rise of AI, the general public is instead burdened with the same kind
of angst that encumbered the chess world in the nineties.  No profession seems
safe from being transformed into a gig (*à la* Uber or Fiverr) or a
popularity contest (*à la* Youtube or Instagram), if not entirely
abolished. There is a profound malaise with AI, and it goes beyond concerns for
the economy.  Citizens do not trust their government make good use of
algorithms.  The recent example of tracking apps for COVID-19 is a good
illustration.  Relying on some algorithm for such a serious question is very
uncomfortable, especially since we know that decisions made by the algorithm
will not be explained and that we will need to accept these decisions without
understanding them.  In addition, citizens are not equipped to verify that
important ethical principles, such as privacy, are enforced where it counts: in
the source code.

If the transition to AI was a success for chess, the underlying reasons are
absent from today's popular software systems.  Imagining and building a
different kind of software industry is now a *sine qua non* condition of
our individual and collective well-being.  Needless to say, it is also quite a
challenge.  For researcher, who will need to find ways to harness the power of
machine learning while producing systems that are intelligible and trustworthy.
And for the State, which will need to create the necessary conditions for the
emergence of true software freedom, e.g. by adding computer programming in
school curriculum and by legislating to make sure that source code is
sufficiently accessible.

The challenge is great, but what we would gain is equally great: a new way
for citizens to participate in public life and defend their rights.  In
trying to embed a notion of common good directly into its software systems,
humanity would gain, through deliberations about source code, nothing
less than a new kind of democracy.

