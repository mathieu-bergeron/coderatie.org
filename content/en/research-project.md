---
title: "Using AI to teach computer programming"
date: 2020-03-29T06:52:00-04:00
draft: false
---

<br>

<center>
<div style="font-size:80%; padding:10px; border:solid pink 2px">
This project has been sumitted for a FRQNT grant.
<br>
Results will be announced in April 2022</div>
</center>

<br>
<br>




### Summary

Artificial intelligence (AI) is both a subjet of wonders and worries.  The
power of these technologies is often impressive, but their trustworthiness is
sometimes lacking, and they mostly operate as black boxes (i.e. theirs
decisions are unexplained).  A promising approach to develop trusted AI is to
generate computer code in order to render their behavior explicit, making that
behavior both understandable and changeable.  Coupled with a better
grasp of the relationship between source code and natural languages
like French, this could represent a major advance in making software more
democratic.

With that mindset, we propose to explore the relationship between natural
language and source code.  In the short term, our project consists of
developping a teaching tool called Texto, which will be based on the notion of
natural language programming, where instructions for the computer to execute
will be specified in French.  The immediate objective is to create a innovative
pedagogical approach for the teaching of computer programming.  In turn, this
tool will allow us to collect concrete data on the relationship between French
and source code.  This kind of data could prouve essential in the creation of
trusted and explainable AI.

To be clear, natural language programming is currently not a general approach
to computer programming.  A naturel text is not precise enough to be considered
as instructions that a computer could execute.  We first need to obtain a
controlled text, which obeys a particular grammar and restricts itself to some
specific vocabulary.  Hence inside Texto, the teaching tool that we propose,
there is a processing pipeline that aims to transform a natural text into a
controlled text.  When that transformation fails, the user is prompted with
suggestions on how to make his/her text more controllable.  Make the
transformation succeeds, the tool visualizes the behavior of the code.  Texto's
pipeline involves various AI techniques including natural language processing
and automated reasnoning. Part of the tool's behavior is dictated by rules that
a teacher has to write in order to create a tutorial for the students.

We think that Texto could become a key teahcing tool at Collège Montmorency,
and possibly in other teaching establishments across the French-speaking world.
We will begin to collect data pairing natural sentences with controlled
sentences and code (note that absolutly no personnal information will be
collected and all consent forms will be approved by the research ethics
committee at Collège Montmorency). Finally, we want to explore the potential of
Texto in terms of accessibility, (e.g. by coupling the tool with speech
recnognition and synthesis) and for other languages than French.
