if [ "$1" = "" ]; then
    echo "usage $0 nom_processus";
    exit 0;
fi;

nom_processus=$1

pourcentage=$(ps -A -o%cpu,cmd | grep $nom_processus | tr -s " " | cut -d" " -f2 | sed "2,$ i+" | tr "\n" " "  | sed aquit | bc )

printf "%s %%\n" $pourcentage
