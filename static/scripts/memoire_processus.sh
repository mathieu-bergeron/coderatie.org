if [ "$1" = "" ]; then
    echo "usage $0 nom_processus";
    exit 0;
fi;

nom_processus=$1

memoire_totale_m=$(free -m | tail -n2 | head -n1 | tr -s " " | cut -d" " -f2)
memoire_totale_100g=$(echo "scale=2; $memoire_totale_m / 1024.0 / 100" | bc)


pourcentage=$(ps -A -o%mem,cmd | grep $nom_processus | tr -s " " | cut -d" " -f2 | sed "2,$ i+" | tr "\n" " "  | sed aquit | bc )
giga=$(echo "scale=2; $pourcentage * $memoire_totale_100g" | bc)

printf "%s %%\n" $pourcentage
printf "%s Go\n" $giga
