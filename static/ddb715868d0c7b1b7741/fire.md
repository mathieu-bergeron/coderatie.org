# Les données de FIRE 

* FIRE: Foundation for Individual Rights and Expression

## Base de données sur les tentatives désinvitations

* Compile les cas où l'on tente de faire annuler une conférence par des actions militantes ou des pressions

* <a target="_blank" href="https://www.thefire.org/research/disinvitation-database/">https://www.thefire.org/research/disinvitation-database/</a>

* <a target="_blank" href="https://www.thefire.org/research/disinvitation-database/users-guide-to-fires-campus-disinvitation-database/">Méthodologie</a>: 
    * depuis 2000
    * soumissions directes à la base de données
    * revues de presse

* Trois types d'université
    * publique
    * privée laïque (*secular*)
    * privée religieuse

* Trois sources de désinvitations
    * à la demande de la droite
    * à la demande de la gauche
    * autre

### Gauche/droite Vs Total

<table>

<tr>
<th>
Gauche/droite
</th>
<th>
Total
</th>
</tr>

<tr>
<td>
<img src="desinvitations_gauche_droite.png" />
</td>
<td>
<img src="desinvitations_total.png" />
</td>
</tr>

</table>

### Qui est désinvité?

TRUC:

* *clic-droit* => *ouvrir l'image dans un nouvel onglet* 
* puis <kbd>Ctrl</kbd>+<kbd>+</kbd> pour zoomer

<br>


<table>

<tr>
<th>
Désinvités par la droite
</th>
<th>
Désinvités par la gauche
</th>
<th>
Autre
</th>
</tr>

<tr>
<td>
<img width="400px" src="conferenciers_droite.png" />
</td>
<td>
<img width="400px" src="conferenciers_gauche.png" />
</td>
<td>
<img width="400px" src="conferenciers_autre.png" />
</td>
</tr>
</table>



### Par type d'université

<table>

<tr>
<th>
Publique
</th>
<th>
Privée laïque
</th>
<th>
Privée religieuse
</th>
</tr>

<tr>
<td>
<img src="desinvitations_gauche_droite_publiques.png" />
</td>
<td>
<img src="desinvitations_gauche_droite_privees_laiques.png" />
</td>
<td>
<img src="desinvitations_gauche_droite_privees_religieuses.png" />
</td>
</tr>

<tr>
<td>
<img src="desinvitations_total_publiques.png" />
</td>
<td>
<img src="desinvitations_total_privees_laiques.png" />
</td>
<td>
<img src="desinvitations_total_privees_religieuses.png" />
</td>
</tr>
</table>



## Base de données sur les menaces de sanctions

* Compile les cas de sanctions ou menaces de sanctions envers un.e prof suite à une prise de parole publique ou privée.

* <a target="_blank" href="https://www.thefire.org/research/scholars-under-fire-database/">https://www.thefire.org/research/scholars-under-fire-database/</a>

* <a target="_blank" href="https://www.thefire.org/research/scholars-under-fire-database/scholars-under-fire-database-guide/">Méthodologie</a>:
    * depuis 2015
    * uniquement à propos d'une  *prise de parole*
        * (exlcut les sanctions pour manquement à l'éthique, harcèlement sexuel, etc.)
    * soumissions directes à la base de données
    * revues de presse
    * contre-vérifications avec 7 autres bases de données similaires

* Trois provenances:
    * à la demande de la droite
    * à la demande de la gauche
    * autre


### Gauche/droite Vs Total

<table>

<tr>
<th>
Gauche/droite
</th>
<th>
Total
</th>
</tr>

<tr>
<td>
<img src="sanctions_gauche_droite.png" />
</td>
<td>
<img src="sanctions_total.png" />
</td>
</tr>

</table>

### Exemples

À la demande de la droite:

* Pour le contenu d'un cours sur le conflit israélo-palestinien
* Suite aux pressions d'une compagnie pétrolière s'opposant aux recherches sur les eaux usées
* Utilisation d'un accessoire phallique lors d'une pièce de théâtre

À la demande de la gauche:

* Pour un lettre d'opinion critique de l'Islam
* Pour avoir critiqué la politique des "safe space" de Yale
* Pour le contenu du cours "Les hommes et la littérature"




