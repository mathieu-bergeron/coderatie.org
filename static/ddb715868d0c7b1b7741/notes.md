# Congédié pour avoir partagé des infos

## Zac Kriegman

* Zac Kriegman partage un article de Roland Fryer

* Pourtant le New York Times a aussi fait un résumé de l'article

https://www.nytimes.com/2016/07/12/upshot/surprising-new-evidence-shows-bias-in-police-use-of-force-but-not-in-shootings.html


## Bo Winegard

https://www.insidehighered.com/news/2020/03/12/assistant-professor-says-hes-been-fired-because-he-dared-talk-about-human-population

## Wang


FIRE: targeting incident

Wang was demoted and censored for publishing a peer-reviewed article critical of affirmative action.

## Mead

Mead faced calls to retract his op-ed piece and peer-reviewed work on poverty and racism.

## Tuvel

Tuvel faced retraction demands and hate mail following her publication of a peer-reviewed article comparing transgenderism to trans-racialism.

## Gilley

Gilley was subject to a petition demanding the retraction of his peer-reviewed paper on the supposed benefits of Western colonialism.

## Cliske

Cliske faced calls for retraction of his peer-reviewed paper on gender dysphoria theory.

## Abrams

Students called for Abrams' tenure to be reviewed over of his 2018 article on the political homogeneity of college administrators.

## Block

Block faced calls for firing over a quote in a NYT article about slavery.

## Burt

Burt resigned from the editorial board of Feminist Criminology amid backlash for her research article on the U.S. Equality Act and women's rights.

## Bleich

Bleich was suspended for saying the \"n\" word while reading an article by Randall Kennedy about whether it's appropriate to use the epithet for pedagogical purposes.

